# Define colour variables
source ~/.bash/colours

# Aliases and shortcut functions
source ~/.bash/aliases

# Load completion scripts
source ~/.bash/completion_git

# Set Environment variables
source ~/.bash/env

# Define PS1
source ~/.bash/ps1_functions
