" Must come first because it changes other options.
if filereadable(expand("~/.vimrc.bundles"))
  source ~/.vimrc.bundles
endif

syntax on                         " Turn on syntax highlighting.
filetype plugin indent on         " Turn on file type detection.

set notimeout
set ttimeout
set timeoutlen=100
set noshowcmd                     " Display incomplete commands.
set showmode                      " Display the mode you're in.
set backspace=indent,eol,start    " Intuitive backspacing.
set hidden                        " Secret sauce handle multiple buffers.
set wildmenu                      " Enhanced command line completion.
set wildmode=list:longest,full    " Complete files like a shell.
set wildcharm=<c-z>               " Complete files like a shell.
set wildignore+=*.o,*.obj,*.pyc,*.DS_Store,*.db " Hide irrelevent matches
set number                        " Show line numbers.
set numberwidth=4                   " 4 characters wide.
set ruler                         " Show cursor position.
set linespace=2                   " Set the space between lines(e.g. leading)

" Show the current line the cursor is on
set cursorline

" Use the dark colors in a theme
set background=dark


" Show 3 lines of context around the cursor.
set scrolloff=3

" Speed up scrolling
set scrolljump=2

set title                         " Set the terminal's title
set visualbell                    " No beeping.

" Backup and Swap
set nobackup                      " Don't make a backup before overwriting a file.
set nowritebackup                 " And again.
set noswapfile

" Visual Column Marker
set colorcolumn=80

" Stop long lines from wrapping
set nowrap                                          

" - tabs
set softtabstop=2
set tabstop=2                     " Global tab width.
set shiftwidth=2                  " And again, related.
set expandtab                     " Use spaces instead of tabs

" - display extra whitespace
set nolist listchars=tab:»·,trail:.


" Automatically set the indent of a new line (local to buffer)
set autoindent

" smartindent (local to buffer)
set smartindent

" Don't redraw screen during macros
" Uncomment if things are running slow
" set lazyredraw

" Improves redrawing for newer computers
set ttyfast

" Make command line two lines high
set cmdheight=2

" Show the status line all the time
set laststatus=2

" Useful status information at bottom of screen
" set statusline=\ ::%f%m%r\ [type=%Y]%=[%03l,%03v][%L]\ 
set statusline=[%n]\ %<%.99f\ %h%w%m%r%y\ %{fugitive#statusline()}%{exists('*CapsLockStatusline')?CapsLockStatusline():''}%=%-16(\ %l,%c-%v\ %)%P

" Search
set incsearch                     " Highlight matches as you type.
set hlsearch                      " Highlight matches.
set ignorecase                    " Case-insensitive searching.
set smartcase                     " But case-sensitive if expression contains a capital letter.

" Window management
set equalalways " Multiple windows, when created, are equal in size

" A more a natural window split 
set splitbelow
set splitright


if &term =~ '256color'
  " Disable Background Color Erase (BCE) so that color schemes
  " work properly when Vim is used inside tmux and GNU screen.
  " See also http://snk.tuxfamily.org/log/vim-256color-bce.html
  set t_ut=
  set t_Co=256
endif

let g:solarized_termcolors=16
" let g:rehash256 = 1
silent! colorscheme solarized

" ******************************************
" Keys custom mappings
" ******************************************

let mapleader = ","

noremap <Leader>i :set list!<CR>  " Toggle invisible chars
noremap <Leader>n :nohlsearch<CR>  " Hide Search

"Vertical split then hop to new buffer
noremap <Leader>v :vsp^M^W^W<cr>
noremap <Leader>h :split^M^W^W<cr>

map <S-Enter> O<ESC> " awesome, inserts new line without going into insert mode
map <Enter> o<ESC>

" Move lines up and down
map <C-j> :m+1<CR>
map <C-k> :m-2<CR>


" PLUG-IN CONFIGURATIONS BELOW
" ******************************************

if executable('ag')
  " Use Ag over Grep
  set grepprg=ag\ --nogroup\ --nocolor

  " Use ag in CtrlP for listing files. Lightning fast and respects .gitignore
  let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'

  " ag is fast enough that CtrlP doesn't need to cache
  let g:ctrlp_use_caching = 0

  " now speed up ack.vim :)
  let g:ackprg = 'ag --nogroup --nocolor --column'

endif


let g:netrw_list_hide='^\.DS_Store$,^.*\.pyc$' " Hide certain files from netrw.

let g:SuperTabContextDefaultCompletionType="<c-p>"

" Super Awesome CTRLP
noremap <Leader>b :CtrlPBuffer<CR>
noremap <Leader>f :CtrlP<CR>
let ctrlp_max_height = 15

" autocomplpop ***************************************************************
" complete option
set complete=.,w,b,u,t,k
let g:AutoComplPop_CompleteOption = '.,w,b,u,t,k'
let g:AutoComplPop_IgnoreCaseOption = 0
let g:AutoComplPop_BehaviorKeywordLength = 2

" local config
if filereadable($HOME . "/.vimrc.local")
  source ~/.vimrc.local
endif
