#!/bin/sh

echo "Please enter irssi name: "
read IRSSI_NAME

echo "Please enter git name: "
read GIT_NAME

echo "Please enter git email: "
read GIT_EMAIL
echo "Installing dotfiles"

# make sure we have any directories that are not in the repo
mkdir -p vim/bundle
#
for name in *; do
  target="$HOME/.$name"
  echo 
  if [ -e "$target" ]; then
    if [ ! -L "$target" ]; then
      echo "WARNING: $target exists but is not a symlink."
      echo "         Delete or backup the file and then run install.sh again"
    else
      echo "$target already exists."
    fi
  else
    if [[ $name != "install.sh" && $name != example_* ]];
    then
      echo "Creating $target"
      ln -sf "$PWD/$name" "$target"
    fi
  fi
done

if [ ! -e ~/.vim/bundle/vundle ]; then
  git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle
fi

# create configs for example_configs
echo "Creating custom configs"
echo "  .irssi/config"
sed -e "s/{{irssi_name}}/$IRSSI_NAME/g;" irssi/example_config > $HOME/.irssi/config

echo "  .gitconfig.local"
sed -e "s/{{GIT_NAME}}/$GIT_NAME/g; s/{{GIT_EMAIL}}/$GIT_EMAIL/g;" example_gitconfig.local > $HOME/.gitconfig.local

# Bundle Up
vim -u ~/.vimrc.bundles +BundleInstall +qall

